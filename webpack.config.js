const { resolve } = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");

module.exports = {
  entry: {
    app: "./src/app.js"
  },
  output: {
    path: resolve("./dist"),
    filename: "bundle.js"
  },

  devtool: "source-map",

  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: [/node_modules/],
        options: {
          presets: ["@babel/preset-react"]
        }
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [
          {
            loader: "file-loader"
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      // From mapbox-gl-js README. Required for non-browserify bundlers (e.g. webpack):
      "mapbox-gl$": resolve("./node_modules/mapbox-gl/dist/mapbox-gl.js")
    }
  },
  plugins: [
    new Dotenv({
      path: "./.env",
      safe: true
    }),
    new HtmlWebpackPlugin({ template: "index.html" })
  ]
};
