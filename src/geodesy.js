function destinationPoint(point, distance, bearing, radius = 6371e3) {
  const δ = distance / radius;
  const θ = toRadians(Number(bearing));

  const φ1 = toRadians(point.lat);
  const λ1 = toRadians(point.lng);

  const sinφ2 =
    Math.sin(φ1) * Math.cos(δ) + Math.cos(φ1) * Math.sin(δ) * Math.cos(θ);
  const φ2 = Math.asin(sinφ2);
  const y = Math.sin(θ) * Math.sin(δ) * Math.cos(φ1);
  const x = Math.cos(δ) - Math.sin(φ1) * sinφ2;
  const λ2 = λ1 + Math.atan2(y, x);

  const lat = toDegrees(φ2);
  const lng = toDegrees(λ2);

  return { lat, lng };
}

function toDegrees(number) {
  return (number * 180) / Math.PI;
}

function toRadians(number) {
  return (number * Math.PI) / 180;
}

module.exports = {
  destinationPoint,
  toDegrees,
  toRadians
};
