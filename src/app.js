import React, { Component } from "react";
import { render } from "react-dom";
import MapGL from "react-map-gl";
import DeckGL, { IconLayer } from "deck.gl";
import { destinationPoint } from "./geodesy";
import planeIcon from "./icons/plane.png";

const MAPBOX_ACCESS_TOKEN = process.env.MAPBOX_ACCESS_TOKEN;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        latitude: 51.47,
        longitude: 0.45,
        zoom: 4,
        bearing: 0,
        pitch: 0
      },
      planes: [],
      selected: {}
    };
    this.loop = null;
    this.animation = null;
    this.frame = null;
    this.then = null;
    this.timer = 10000;
  }

  componentDidMount() {
    this.fetchPlanes();
  }

  componentWillUnmount() {
    clearTimeout(this.loop);
  }

  fetchPlanes() {
    fetch("https://opensky-network.org/api/states/all")
      .then(res => res.json())
      .then(json => {
        this.setState(
          {
            planes: json.states
              .filter(plane => plane[5] !== null || plane[6] !== null)
              .map(plane => {
                return {
                  icao: plane[0],
                  cs: plane[1],
                  lng: plane[5],
                  lat: plane[6],
                  speed: plane[9],
                  brg: -plane[10],
                  alt: plane[13]
                };
              })
          },
          () => {
            this.frame = 0;
            if (this.animation) {
              cancelAnimationFrame(this.animation);
            }
            this.then = 0;
            this.animation = requestAnimationFrame(now => this.animate(now));
            this.loop = setTimeout(() => this.fetchPlanes(), this.timer);
          }
        );
      })
      .catch(err => console.log(err));
  }

  fetchPlane(plane) {
    fetch(`https://opensky-network.org/api/tracks?icao24=${plane.icao}`)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        // this.setState({
        //   plane: json.states
        //     .filter(plane => plane[5] !== null || plane[6] !== null)
        //     .map(plane => {
        //       return {
        //         cs: plane[1],
        //         lng: plane[5],
        //         lat: plane[6],
        //         speed: plane[9],
        //         brg: -plane[10],
        //         alt: plane[13]
        //       };
        //     })
        // });
      })
      .catch(err => console.log(err));
  }

  animate(now) {
    // Determine how many fps are used
    now *= 0.001;
    let delta = now - this.then;
    this.then = now;
    let planes = this.state.planes.map(plane => {
      const { lat, lng } = destinationPoint(
        { lat: plane.lat, lng: plane.lng },
        plane.speed * delta,
        -plane.brg
      );

      return {
        ...plane,
        lat,
        lng
      };
    });
    this.setState({ planes });
    requestAnimationFrame(now => this.animate(now));
  }

  onSelect(info, event) {
    this.fetchPlane(info.object);
  }

  render() {
    const layers = [
      new IconLayer({
        id: "planes",
        data: this.state.planes,
        iconAtlas: planeIcon,
        iconMapping: {
          plane: {
            x: 0,
            y: 0,
            width: 256,
            height: 256,
            mask: false
          }
        },
        pickable: true,
        onClick: (info, event) => this.onSelect(info, event),
        sizeUnits: "pixels",
        sizeScale: 1,
        getPosition: d => [d.lng, d.lat, 0],
        getIcon: d => "plane",
        getColor: [0, 0, 0, 255],
        getSize: 35,
        getAngle: d => d.brg + this.state.viewport.bearing
      })
    ];
    return (
      <MapGL
        {...this.state.viewport}
        width="100vw"
        height="100vh"
        mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
        mapStyle="mapbox://styles/mapbox/light-v9"
        onViewportChange={viewport => {
          this.setState({
            viewport: {
              ...this.state.viewport,
              ...viewport
            }
          });
        }}
      >
        <DeckGL
          initialViewState={this.state.viewport}
          controller={true}
          layers={layers}
        />
      </MapGL>
    );
  }
}

render(<App />, document.body.appendChild(document.createElement("div")));
